#!/usr/bin/env python
from smat_be import APP
from unittest import TestCase
from fastapi.testclient import TestClient
import os


class TestBase(TestCase):
    """forms the base test_client app instance.

    to be inherited by tests of all routes.
    """

    def setUp(self) -> None:
        """Runs before the test."""

        self.APP = TestClient(APP)
