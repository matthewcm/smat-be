#!/usr/bin/env python
"""The SMAT backend APP just makes Elasticsearch's easier to work with.

While retaining all the complexity/perplexity of Elasticsearch output.
"""
import datetime
from json.decoder import JSONDecodeError
from enum import Enum
from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI, Query, HTTPException
from fastapi.openapi.utils import get_openapi
from smat_be.analysis.bot_detection import parse_twitter_source
from smat_be.constants import UNTIL, SINCE
from smat_be.utils import mk_since_until, map_site_to_default_agg_by
from smat_be.queries import (
    content_query,
    time_series_query,
    activity_query,
)

APP = FastAPI()
APP.add_middleware(CORSMiddleware, allow_origins=["*"])


class Site(str, Enum):
    """Site enumerator."""

    reddit = "reddit"
    twitter = "twitter"
    eight_kun = "8kun"
    four_chan = "4chan"
    gab = "gab"
    parler = "parler"


@APP.get("/content", status_code=200)
def content(
    term: str,
    limit: int = Query(10),
    site: Site = Query("reddit"),
    since: datetime.date = Query(SINCE),
    until: datetime.date = Query(UNTIL),
):
    """Retrieve a list of post objects."""
    _since = mk_since_until(since)
    _until = mk_since_until(until)

    data_response, created_key, content_key = content_query(
        term=term, site=site.value, limit=limit, since=_since, until=_until
    )

    if data_response.status_code == 200:
        try:
            _ = data_response.json()["hits"]["hits"]
        except JSONDecodeError as error:
            raise HTTPException(
                status_code=500,
                detail=f"Data store responded with a 200, but cant extract json: {data_response.text}",  # pylint: disable=C0301
            ) from error

        except KeyError as error:
            raise HTTPException(
                status_code=500,
                detail=f"Data store responded with a 200, but cant parse aggregations: {data_response.text}",  # pylint: disable=C0301
            ) from error

        response = {
            **{"created_key": created_key, "content_key": content_key},
            **data_response.json(),
        }

    else:
        raise HTTPException(
            status_code=500, detail=f"Data store error: {data_response.text}"
        )

    return response


@APP.get("/timeseries", status_code=200)
def timeseries(
    term: str,
    interval: str = Query("day"),
    site: Site = Query("reddit"),
    since: datetime.date = Query(SINCE),
    until: datetime.date = Query(UNTIL),
    changepoint: bool = Query(False),
):
    """Retrieve a time series."""
    # returns a tuple (y, m, d)
    _since = mk_since_until(since)
    _until = mk_since_until(until)

    data_response, created_key = time_series_query(
        term=term,
        site=site.value,
        interval=interval,
        since=_since,
        until=_until,
        changepoint=changepoint,
    )

    if data_response.status_code == 200:
        try:
            _ = data_response.json()["aggregations"][created_key]["buckets"]
        except JSONDecodeError as error:
            raise HTTPException(
                status_code=500,
                detail=f"Data store responded with a 200, but cant extract json: {data_response.text}",  # pylint: disable=C0301
            ) from error

        except KeyError as error:
            raise HTTPException(
                status_code=500,
                detail=f"Data store responded with a 200, but cant parse aggregations: {data_response.text}",  # pylint: disable=C0301
            ) from error

        response = {**{"created_key": created_key}, **data_response.json()}  # type: ignore
    else:
        raise HTTPException(
            status_code=500, detail=f"Data store error: {data_response.text}"
        )
    return response


@APP.get("/activity", status_code=200)
def activity(
    term: str,
    agg_by: str = Query(None),
    site: Site = Query("reddit"),
    since: datetime.date = Query(SINCE),
    until: datetime.date = Query(UNTIL),
):
    """Aggregate over a particular field on a single site."""
    _since = mk_since_until(since)
    _until = mk_since_until(until)
    agg_by = agg_by or map_site_to_default_agg_by(site.value)

    data_response = activity_query(
        term=term, site=site.value, agg_by=agg_by, since=_since, until=_until
    )

    if data_response.status_code == 200:
        try:
            _ = data_response.json()["aggregations"][agg_by]["buckets"]
        except JSONDecodeError as error:
            raise HTTPException(
                status_code=500,
                detail=f"Data store responded with a 200, but cant extract json: {data_response.text}",  # pylint: disable=C0301
            ) from error

        except KeyError as error:
            raise HTTPException(
                status_code=500,
                detail=f"Data store responded with a 200, but cant parse aggregations: {data_response.text}",  # pylint: disable=C0301
            ) from error

        else:
            response = data_response.json()
            response["aggby_key"] = agg_by

    else:
        raise HTTPException(
            status_code=500, detail=f"Data store error: {data_response.text}"
        )

    if site == "twitter" and agg_by == "source":
        for bucket in response["aggregations"]["source"]["buckets"]:
            bucket["key"] = parse_twitter_source(bucket["key"])

    return response


def custom_openapi():
    """Create custom spec doc."""
    if APP.openapi_schema:
        return APP.openapi_schema
    openapi_schema = get_openapi(
        title="SMAT",
        version="0.0.1",
        description="SMAT Backend API",
        routes=APP.routes,
    )
    openapi_schema["info"]["x-logo"] = {
        "url": "https://gitlab.com/smat-project/backends/smat-be/-/raw/master/logo.png"
    }
    APP.openapi_schema = openapi_schema
    return APP.openapi_schema


APP.openapi = custom_openapi  # type: ignore # pylint: disable=C0301
