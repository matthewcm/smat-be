#!/usr/bin/env python
"""Various bot detection analyses."""
from bs4 import BeautifulSoup


def parse_twitter_source(text):
    """Parse twitter response HTML."""
    soup = BeautifulSoup(text, "html.parser")
    return soup.find("a").text
