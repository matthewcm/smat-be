#!/usr/bin/env python
"""Utility functions for the smat back end."""
from typing import Dict, Any, Tuple
from datetime import datetime, date
from collections import defaultdict
from smat_be.settings import (
    REDDIT_COMMENT_SEARCH,
    TWITTER_VERIFIED_SEARCH,
    GAB_POST_SEARCH,
    FOUR_CHAN_SEARCH,
    EIGHT_CHAN_SEARCH,
    PARLER_POST_SEARCH
)


def dict_factory() -> Dict[str, Any]:
    """Create a nested dict for json purposes."""
    return defaultdict(
        lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))
    )


# TODO: use python date parser # pylint: disable=W0511
def mk_since_until(date_input: date) -> Tuple[int, int, int]:
    """Transform input into needed format."""
    return int(date_input.year), int(date_input.month), int(date_input.day)


def dt_to_epoch(date_input: datetime) -> str:
    """Datetime object to epoch time."""
    return date_input.strftime("%s")


def map_site_to_default_agg_by(site: str) -> str:
    """Take in a site name, returns a default agg_by field."""
    abm = {
        "reddit": "author",
        "twitter": "screen_name",
        "gab": "account.acct",
        "4chan": "name",
        "8kun": "name",
        "parler": "username"
    }
    return abm[site]


def map_site_to_query(site: str) -> Tuple:
    """Map a site string to query args."""
    sqm = {
        "reddit": {
            "created": "created_utc",
            "url": REDDIT_COMMENT_SEARCH,
            "content": "body",
            "dt_format": "epoch",
        },
        "4chan": {"created": "now", "url": FOUR_CHAN_SEARCH, "content": "html_parsed_com"},
        "8kun": {"created": "time", "url": EIGHT_CHAN_SEARCH, "content": "htmlparsedcom"},
        "twitter": {
            "created": "created_at",
            "url": TWITTER_VERIFIED_SEARCH,
            "content": "text",
        },
        "gab": {"created": "createdat", "url": GAB_POST_SEARCH, "content": "content"},
        "parler": {
            "created": "createdAtformatted",
            "url": PARLER_POST_SEARCH,
            "content": "bodywithurls"
        },
    }
    msqm = sqm[site]
    return (msqm["created"], msqm["content"], msqm["url"])
