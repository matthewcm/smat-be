#!/usr/bin/env python

"""Define some constants for the app."""
from datetime import date
from dateutil.relativedelta import relativedelta

HEADERS = {
    "Content-Type": "application/json",
    "Accept-Encoding": "deflate, compress, gzip",
}


TODAY = date.today()
UNTIL = TODAY
SINCE = TODAY - relativedelta(months=2)
