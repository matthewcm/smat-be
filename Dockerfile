FROM python:3.7

EXPOSE $PORT

COPY . .

RUN pip install fastapi uvicorn

RUN pip install -e .

CMD uvicorn smat_be.main:APP --host 0.0.0.0 --port $PORT
